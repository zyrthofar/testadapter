package testadapter

import (
	"context"
	"database/sql"
	"testing"

	"github.com/upper/db/v4"
)

// SQL defines an upper.io sql object used in tests to simulate a database.
type SQL struct {
	t *testing.T

	selectorAllExpectations []*SelectorAllExpectation
	selectorOneExpectations []*SelectorOneExpectation
}

// Select creates a new test selector.
func (s *SQL) Select(args ...interface{}) db.Selector {
	return &Selector{
		sql: s,
		currentArgs: &selectorArgs{
			selectArgs: args,
		},
		t: s.t,
	}
}

// SelectFrom creates a new test selector.
func (s *SQL) SelectFrom(args ...interface{}) db.Selector {
	return &Selector{
		sql: s,
		currentArgs: &selectorArgs{
			selectFromArgs: args,
		},
		t: s.t,
	}
}

// InsertInto is not implemented yet.
func (s *SQL) InsertInto(table string) db.Inserter {
	panic(ErrUnimplemented)
}

// Update is not implemented yet.
func (s *SQL) Update(table string) db.Updater {
	panic(ErrUnimplemented)
}

// DeleteFrom is not implemented yet.
func (s *SQL) DeleteFrom(table string) db.Deleter {
	panic(ErrUnimplemented)
}

// Exec is not implemented yet.
func (s *SQL) Exec(query interface{}, args ...interface{}) (sql.Result, error) {
	panic(ErrUnimplemented)
}

// ExecContext is not implemented yet.
func (s *SQL) ExecContext(ctx context.Context, query interface{}, args ...interface{}) (sql.Result, error) {
	panic(ErrUnimplemented)
}

// Prepare is not implemented yet.
func (s *SQL) Prepare(query interface{}) (*sql.Stmt, error) {
	panic(ErrUnimplemented)
}

// PrepareContext is not implemented yet.
func (s *SQL) PrepareContext(ctx context.Context, query interface{}) (*sql.Stmt, error) {
	panic(ErrUnimplemented)
}

// Query is not implemented yet.
func (s *SQL) Query(query interface{}, args ...interface{}) (*sql.Rows, error) {
	panic(ErrUnimplemented)
}

// QueryContext is not implemented yet.
func (s *SQL) QueryContext(ctx context.Context, query interface{}, args ...interface{}) (*sql.Rows, error) {
	panic(ErrUnimplemented)
}

// QueryRow is not implemented yet.
func (s *SQL) QueryRow(query interface{}, args ...interface{}) (*sql.Row, error) {
	panic(ErrUnimplemented)
}

// QueryRowContext is not implemented yet.
func (s *SQL) QueryRowContext(ctx context.Context, query interface{}, args ...interface{}) (*sql.Row, error) {
	panic(ErrUnimplemented)
}

// Iterator is not implemented yet.
func (s *SQL) Iterator(query interface{}, args ...interface{}) db.Iterator {
	panic(ErrUnimplemented)
}

// IteratorContext is not implemented yet.
func (s *SQL) IteratorContext(ctx context.Context, query interface{}, args ...interface{}) db.Iterator {
	panic(ErrUnimplemented)
}

// NewIterator is not implemented yet.
func (s *SQL) NewIterator(rows *sql.Rows) db.Iterator {
	panic(ErrUnimplemented)
}

// NewIteratorContext is not implemented yet.
func (s *SQL) NewIteratorContext(ctx context.Context, rows *sql.Rows) db.Iterator {
	panic(ErrUnimplemented)
}

var _ = db.SQL(&SQL{})
