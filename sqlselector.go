package testadapter

import (
	"context"
	"database/sql"
	"fmt"
	"reflect"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/upper/db/v4"
)

// Selector represents a SELECT statement.
type Selector struct {
	sql         *SQL
	currentArgs *selectorArgs
	t           *testing.T
}

// Columns adds an argument expectation to the selector.
func (s *Selector) Columns(args ...interface{}) db.Selector {
	s.currentArgs.columnArgs = append(s.currentArgs.columnArgs, args)
	return s
}

// From adds an argument expectation to the selector.
func (s *Selector) From(args ...interface{}) db.Selector {
	s.currentArgs.fromArgs = append(s.currentArgs.fromArgs, args)
	return s
}

// Distinct adds an argument expectation to the selector.
func (s *Selector) Distinct(args ...interface{}) db.Selector {
	s.currentArgs.distinctArgs = append(s.currentArgs.distinctArgs, args)
	return s
}

// As adds an argument expectation to the selector.
func (s *Selector) As(arg string) db.Selector {
	s.currentArgs.asArgs = append(s.currentArgs.asArgs, arg)
	return s
}

// Where adds an argument expectation to the selector.
func (s *Selector) Where(args ...interface{}) db.Selector {
	s.currentArgs.whereArgs = append(s.currentArgs.whereArgs, args)
	return s
}

// And adds an argument expectation to the selector.
func (s *Selector) And(args ...interface{}) db.Selector {
	s.currentArgs.andArgs = append(s.currentArgs.andArgs, args)
	return s
}

// GroupBy adds an argument expectation to the selector.
func (s *Selector) GroupBy(args ...interface{}) db.Selector {
	s.currentArgs.groupByArgs = append(s.currentArgs.groupByArgs, args)
	return s
}

// OrderBy adds an argument expectation to the selector.
func (s *Selector) OrderBy(args ...interface{}) db.Selector {
	s.currentArgs.orderByArgs = append(s.currentArgs.orderByArgs, args)
	return s
}

// Join adds an argument expectation to the selector.
func (s *Selector) Join(args ...interface{}) db.Selector {
	s.currentArgs.joinArgs = append(s.currentArgs.joinArgs, args)
	return s
}

// FullJoin adds an argument expectation to the selector.
func (s *Selector) FullJoin(args ...interface{}) db.Selector {
	s.currentArgs.fullJoinArgs = append(s.currentArgs.fullJoinArgs, args)
	return s
}

// CrossJoin adds an argument expectation to the selector.
func (s *Selector) CrossJoin(args ...interface{}) db.Selector {
	s.currentArgs.crossJoinArgs = append(s.currentArgs.crossJoinArgs, args)
	return s
}

// RightJoin adds an argument expectation to the selector.
func (s *Selector) RightJoin(args ...interface{}) db.Selector {
	s.currentArgs.rightJoinArgs = append(s.currentArgs.rightJoinArgs, args)
	return s
}

// LeftJoin adds an argument expectation to the selector.
func (s *Selector) LeftJoin(args ...interface{}) db.Selector {
	s.currentArgs.leftJoinArgs = append(s.currentArgs.leftJoinArgs, args)
	return s
}

// Using adds an argument expectation to the selector.
func (s *Selector) Using(args ...interface{}) db.Selector {
	s.currentArgs.usingArgs = append(s.currentArgs.usingArgs, args)
	return s
}

// On adds an argument expectation to the selector.
func (s *Selector) On(args ...interface{}) db.Selector {
	s.currentArgs.onArgs = append(s.currentArgs.onArgs, args)
	return s
}

// Limit adds an argument expectation to the selector.
func (s *Selector) Limit(arg int) db.Selector {
	s.currentArgs.limitArgs = append(s.currentArgs.limitArgs, arg)
	return s
}

// Offset adds an argument expectation to the selector.
func (s *Selector) Offset(arg int) db.Selector {
	s.currentArgs.offsetArgs = append(s.currentArgs.offsetArgs, arg)
	return s
}

// Amend adds an argument expectation to the selector.
func (s *Selector) Amend(arg func(string) string) db.Selector {
	s.currentArgs.amendArgs = append(s.currentArgs.amendArgs, arg)
	return s
}

// Paginate is not implemented yet.
func (s *Selector) Paginate(uint) db.Paginator {
	panic(ErrUnimplemented)
}

// Iterator is not implemented yet.
func (s *Selector) Iterator() db.Iterator {
	panic(ErrUnimplemented)
}

// IteratorContext is not implemented yet.
func (s *Selector) IteratorContext(ctx context.Context) db.Iterator {
	panic(ErrUnimplemented)
}

// Prepare is not implemented yet.
func (s *Selector) Prepare() (*sql.Stmt, error) {
	panic(ErrUnimplemented)
}

// PrepareContext is not implemented yet.
func (s *Selector) PrepareContext(context.Context) (*sql.Stmt, error) {
	panic(ErrUnimplemented)
}

// Query is not implemented yet.
func (s *Selector) Query() (*sql.Rows, error) {
	panic(ErrUnimplemented)
}

// QueryContext is not implemented yet.
func (s *Selector) QueryContext(context.Context) (*sql.Rows, error) {
	panic(ErrUnimplemented)
}

// QueryRow is not implemented yet.
func (s *Selector) QueryRow() (*sql.Row, error) {
	panic(ErrUnimplemented)
}

// QueryRowContext is not implemented yet.
func (s *Selector) QueryRowContext(ctx context.Context) (*sql.Row, error) {
	panic(ErrUnimplemented)
}

// All returns the first matching expectation from the list, replacing the specified slice with the
// returned items.
//
// This function panics with an ErrNoMatchingExpectations error if there are no such expectations.
func (s *Selector) All(slice interface{}) error {
	for i, e := range s.sql.selectorAllExpectations {
		if s.matchesArgs(e.expectedArgs) {
			if e.ReturnedSlice != nil && cmp.Equal(e.ExpectedSlice, slice) {
				reflect.ValueOf(slice).Elem().Set(reflect.ValueOf(e.ReturnedSlice))
			}

			s.sql.selectorAllExpectations = append(s.sql.selectorAllExpectations[:i], s.sql.selectorAllExpectations[i+1:]...)

			return e.ReturnedError
		}
	}

	panic(fmt.Errorf("error matching selector.All: %w", ErrNoMatchingExpectations))
}

// ExpectAllArgs adds an expectation for the All method, by separate arguments.
func (s *Selector) ExpectAllArgs(slice interface{}, retSlice interface{}, retError error) {
	e := &SelectorAllExpectation{
		ExpectedSlice: slice,
		ReturnedSlice: retSlice,
		ReturnedError: retError,
	}

	s.ExpectAll(e)
}

// ExpectAll adds an expectation for the All method.
func (s *Selector) ExpectAll(e *SelectorAllExpectation) {
	if e != nil {
		e.expectedArgs = s.currentArgs
		s.sql.selectorAllExpectations = append(s.sql.selectorAllExpectations, e)
	}
}

// One returns the first matching expectation from the list, replacing the specified item with the
// returned one.
//
// This function panics with an ErrNoMatchingExpectations error if there are no such expectations.
func (s *Selector) One(item interface{}) error {
	for i, e := range s.sql.selectorOneExpectations {
		if s.matchesArgs(e.expectedArgs) && cmp.Equal(e.ExpectedItem, item) {
			if e.ReturnedItem != nil {
				reflect.ValueOf(item).Elem().Set(reflect.Indirect(reflect.ValueOf(e.ReturnedItem)))
			}

			s.sql.selectorOneExpectations = append(s.sql.selectorOneExpectations[:i], s.sql.selectorOneExpectations[i+1:]...)

			return e.ReturnedError
		}
	}

	panic(fmt.Errorf("error matching selector.One: %w", ErrNoMatchingExpectations))
}

// ExpectOneArgs adds an expectation for the One method, by separate arguments.
func (s *Selector) ExpectOneArgs(item interface{}, retItem interface{}, retError error) {
	e := &SelectorOneExpectation{
		ExpectedItem:  item,
		ReturnedItem:  retItem,
		ReturnedError: retError,
	}

	s.ExpectOne(e)
}

// ExpectOne adds an expectation for the One method.
func (s *Selector) ExpectOne(e *SelectorOneExpectation) {
	if e != nil {
		e.expectedArgs = s.currentArgs
		s.sql.selectorOneExpectations = append(s.sql.selectorOneExpectations, e)
	}
}

// String is not implemented yet.
func (s *Selector) String() string {
	panic(ErrUnimplemented)
}

// Arguments is not implemented yet.
func (s *Selector) Arguments() []interface{} {
	panic(ErrUnimplemented)
}

var _ = db.Selector(&Selector{})

func (s *Selector) matchesArgs(expectedArgs *selectorArgs) bool {
	return cmp.Equal(expectedArgs, s.currentArgs, cmp.AllowUnexported(selectorArgs{}, db.RawExpr{}))
}

type selectorArgs struct {
	selectArgs     []interface{}
	selectFromArgs []interface{}
	columnArgs     [][]interface{}
	fromArgs       [][]interface{}
	distinctArgs   [][]interface{}
	asArgs         []string
	whereArgs      [][]interface{}
	andArgs        [][]interface{}
	groupByArgs    [][]interface{}
	orderByArgs    [][]interface{}
	joinArgs       [][]interface{}
	fullJoinArgs   [][]interface{}
	crossJoinArgs  [][]interface{}
	rightJoinArgs  [][]interface{}
	leftJoinArgs   [][]interface{}
	usingArgs      [][]interface{}
	onArgs         [][]interface{}
	limitArgs      []int
	offsetArgs     []int
	amendArgs      []func(string) string
}

// SelectorAllExpectation defines the expected and returned values for the All method.
type SelectorAllExpectation struct {
	expectedArgs  *selectorArgs
	ExpectedSlice interface{}
	ReturnedSlice interface{}
	ReturnedError error
}

// SelectorOneExpectation defines the expected and returned values for the One method.
type SelectorOneExpectation struct {
	expectedArgs  *selectorArgs
	ExpectedItem  interface{}
	ReturnedItem  interface{}
	ReturnedError error
}
