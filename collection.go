package testadapter

import (
	"fmt"
	"reflect"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/upper/db/v4"
)

// Collection defines an upper.io collection used in tests to simulate a database.
type Collection struct {
	session *Session
	name    string
	t       *testing.T

	countExpectations           []*CollectionCountExpectation
	existsExpectations          []*CollectionExistsExpectation
	insertExpectations          []*CollectionInsertExpectation
	insertReturningExpectations []*CollectionInsertReturningExpectation
	updateReturningExpectations []*CollectionUpdateReturningExpectation
	truncateExpectations        []*CollectionTruncateExpectation

	resultCountExpectations  []*ResultCountExpectation
	resultExistsExpectations []*ResultExistsExpectation
	resultAllExpectations    []*ResultAllExpectation
	resultOneExpectations    []*ResultOneExpectation
	resultUpdateExpectations []*ResultUpdateExpectation
	resultDeleteExpectations []*ResultDeleteExpectation
}

func NewCollection(s *Session, name string) *Collection {
	return s.Collection(name).(*Collection)
}

// Name returns test collection name.
func (c *Collection) WithResult(result db.Result) *Result {
	r := result.(*Result)
	r.collection = c
	r.t = c.t

	return r
}

// Name returns test collection name.
func (c *Collection) Name() string {
	return c.name
}

// Session returns the current test session.
func (c *Collection) Session() db.Session {
	return c.session
}

// Find creates a new test result.
func (c *Collection) Find(args ...interface{}) db.Result {
	return &Result{
		collection: c,
		currentArgs: &resultArgs{
			findArgs: args,
		},
		t: c.t,
	}
}

// Count returns the first expectation from the list.
//
// This function panics with an ErrNoMatchingExpectations error if there are no such expectations.
func (c *Collection) Count() (uint64, error) {
	if len(c.countExpectations) == 0 {
		panic(fmt.Errorf("error matching collection.Count: %w", ErrNoMatchingExpectations))
	}

	e := c.countExpectations[0]
	c.countExpectations = c.countExpectations[1:]

	return e.ReturnedResult, e.ReturnedError
}

// ExpectCountArgs adds an expectation for the Count method, by separate arguments.
func (c *Collection) ExpectCountArgs(retResult uint64, retError error) {
	e := &CollectionCountExpectation{
		ReturnedResult: retResult,
		ReturnedError:  retError,
	}

	c.ExpectCount(e)
}

// ExpectCount adds an expectation for the Count method.
func (c *Collection) ExpectCount(e *CollectionCountExpectation) {
	if e != nil {
		c.countExpectations = append(c.countExpectations, e)
	}
}

// Exists returns the first expectation from the list.
//
// This function panics with an ErrNoMatchingExpectations error if there are no such expectations.
func (c *Collection) Exists() (bool, error) {
	if len(c.existsExpectations) == 0 {
		panic(fmt.Errorf("error matching collection.Exists: %w", ErrNoMatchingExpectations))
	}

	e := c.existsExpectations[0]
	c.existsExpectations = c.existsExpectations[1:]

	return e.ReturnedResult, e.ReturnedError
}

// ExpectExistsArgs adds an expectation for the Exists method, by separate arguments.
func (c *Collection) ExpectExistsArgs(retResult bool, retError error) {
	e := &CollectionExistsExpectation{
		ReturnedResult: retResult,
		ReturnedError:  retError,
	}

	c.ExpectExists(e)
}

// ExpectExists adds an expectation for the Exists method.
func (c *Collection) ExpectExists(e *CollectionExistsExpectation) {
	if e != nil {
		c.existsExpectations = append(c.existsExpectations, e)
	}
}

// Insert returns the first matching expectation from the list.
//
// This function panics with an ErrNoMatchingExpectations error if there are no such expectations.
func (c *Collection) Insert(item interface{}) (db.InsertResult, error) {
	for i, e := range c.insertExpectations {
		if cmp.Equal(e.ExpectedItem, item) {
			c.insertExpectations = append(c.insertExpectations[:i], c.insertExpectations[i+1:]...)
			return db.NewInsertResult(e.ReturnedID), e.ReturnedError
		}
	}

	panic(fmt.Errorf("error matching collection.Insert: %w", ErrNoMatchingExpectations))
}

// ExpectInsertArgs adds an expectation for the Insert method, by separate arguments.
func (c *Collection) ExpectInsertArgs(item interface{}, retID interface{}, retError error) {
	e := &CollectionInsertExpectation{
		ExpectedItem:  item,
		ReturnedID:    retID,
		ReturnedError: retError,
	}

	c.ExpectInsert(e)
}

// ExpectInsert adds an expectation for the Insert method.
func (c *Collection) ExpectInsert(e *CollectionInsertExpectation) {
	if e != nil {
		c.insertExpectations = append(c.insertExpectations, e)
	}
}

// InsertReturning returns the first matching expectation from the list.
//
// This function panics with an ErrNoMatchingExpectations error if there are no such expectations.
func (c *Collection) InsertReturning(item interface{}) error {
	for i, e := range c.insertReturningExpectations {
		if cmp.Equal(e.ExpectedItem, item) {
			if e.ReturnedItem != nil {
				reflect.ValueOf(item).Elem().Set(reflect.Indirect(reflect.ValueOf(e.ReturnedItem)))
			}

			c.insertReturningExpectations = append(c.insertReturningExpectations[:i], c.insertReturningExpectations[i+1:]...)

			return e.ReturnedError
		}
	}

	panic(fmt.Errorf("error matching collection.InsertReturning: %w", ErrNoMatchingExpectations))
}

// ExpectInsertReturningArgs adds an expectation for the InsertReturning method, by separate arguments.
func (c *Collection) ExpectInsertReturningArgs(item interface{}, retItem interface{}, retError error) {
	e := &CollectionInsertReturningExpectation{
		ExpectedItem:  item,
		ReturnedItem:  retItem,
		ReturnedError: retError,
	}

	c.ExpectInsertReturning(e)
}

// ExpectInsertReturning adds an expectation for the InsertReturning method.
func (c *Collection) ExpectInsertReturning(e *CollectionInsertReturningExpectation) {
	if e != nil {
		c.insertReturningExpectations = append(c.insertReturningExpectations, e)
	}
}

// UpdateReturning returns the first matching expectation from the list.
//
// This function panics with an ErrNoMatchingExpectations error if there are no such expectations.
func (c *Collection) UpdateReturning(item interface{}) error {
	for i, e := range c.updateReturningExpectations {
		if cmp.Equal(e.ExpectedItem, item) {
			if e.ReturnedItem != nil {
				reflect.ValueOf(item).Elem().Set(reflect.Indirect(reflect.ValueOf(e.ReturnedItem)))
			}

			c.updateReturningExpectations = append(c.updateReturningExpectations[:i], c.updateReturningExpectations[i+1:]...)

			return e.ReturnedError
		}
	}

	panic(fmt.Errorf("error matching collection.UpdateReturning: %w", ErrNoMatchingExpectations))
}

// ExpectUpdateReturningArgs adds an expectation for the UpdateReturning method, by separate arguments.
func (c *Collection) ExpectUpdateReturningArgs(item interface{}, retItem interface{}, retError error) {
	e := &CollectionUpdateReturningExpectation{
		ExpectedItem:  item,
		ReturnedItem:  retItem,
		ReturnedError: retError,
	}

	c.ExpectUpdateReturning(e)
}

// ExpectUpdateReturning adds an expectation for the UpdateReturning method.
func (c *Collection) ExpectUpdateReturning(e *CollectionUpdateReturningExpectation) {
	if e != nil {
		c.updateReturningExpectations = append(c.updateReturningExpectations, e)
	}
}

// Truncate returns the first expectation from the list.
//
// This function panics with an ErrNoMatchingExpectations error if there are no such expectations.
func (c *Collection) Truncate() error {
	if len(c.truncateExpectations) == 0 {
		panic(fmt.Errorf("error matching collection.Truncate: %w", ErrNoMatchingExpectations))
	}

	e := c.truncateExpectations[0]
	c.truncateExpectations = c.truncateExpectations[1:]

	return e.ReturnedError
}

// ExpectTruncateArgs adds an expectation for the Truncate method, by separate arguments.
func (c *Collection) ExpectTruncateArgs(retError error) {
	e := &CollectionTruncateExpectation{
		ReturnedError: retError,
	}

	c.ExpectTruncate(e)
}

// ExpectTruncate adds an expectation for the Truncate method.
func (c *Collection) ExpectTruncate(e *CollectionTruncateExpectation) {
	if e != nil {
		c.truncateExpectations = append(c.truncateExpectations, e)
	}
}

var _ = db.Collection(&Collection{})

// CollectionCountExpectation defines the returned values for the Count method.
type CollectionCountExpectation struct {
	ReturnedResult uint64
	ReturnedError  error
}

// CollectionExistsExpectation defines the returned values for the Exists method.
type CollectionExistsExpectation struct {
	ReturnedResult bool
	ReturnedError  error
}

// CollectionInsertExpectation defines the expected and returned values for the Insert method.
type CollectionInsertExpectation struct {
	ExpectedItem  interface{}
	ReturnedID    interface{}
	ReturnedError error
}

// CollectionInsertReturningExpectation defines the expected and returned values for the
// InsertReturning method.
type CollectionInsertReturningExpectation struct {
	ExpectedItem  interface{}
	ReturnedItem  interface{}
	ReturnedError error
}

// CollectionUpdateReturningExpectation defines the expected and returned values for the
// UpdateReturning method.
type CollectionUpdateReturningExpectation struct {
	ExpectedItem  interface{}
	ReturnedItem  interface{}
	ReturnedError error
}

// CollectionTruncateExpectation defines the returned values for the Truncate method.
type CollectionTruncateExpectation struct {
	ReturnedError error
}
