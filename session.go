package testadapter

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"reflect"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"github.com/upper/db/v4"
)

// Session defines an upper.io session used in tests to simulate a database.
type Session struct {
	collections map[string]*Collection
	sql         *SQL
	t           *testing.T

	saveExpectations   []*SessionSaveExpectation
	getExpectations    []*SessionGetExpectation
	deleteExpectations []*SessionDeleteExpectation
}

// NewSession creates a new test session.
func NewSession(t *testing.T) *Session {
	return &Session{
		collections: make(map[string]*Collection),
		sql:         &SQL{t: t},
		t:           t,
	}
}

// Name returns the name of the test adapter.
func (s *Session) Name() string {
	return "test"
}

// Collection returns the test collection by the specified name, creating it if needed.
func (s *Session) Collection(name string) db.Collection {
	coll, ok := s.collections[name]
	if !ok {
		coll = &Collection{
			session: s,
			name:    name,
			t:       s.t,
		}

		s.collections[name] = coll
	}

	return coll
}

// SQL returns a special interface for SQL databases.
func (s *Session) SQL() db.SQL {
	return s.sql
}

// Get tries to find an expectation, matching against both record and cond arguments.
//
// If a match is found, record will be replaced with the match's ReturnedRecord, and ReturnedError
// will be returned.
//
//     getExpt := &testadapter.SessionGetExpectation{
//         ExpectedRecord: &record{},
//         ExpectedCond:   db.Cond{"id": 1},
//         ReturnedRecord: &record{ID: 1},
//         ReturnedError:  nil,
//     }
//
//     sess := testadapter.NewSession(t)
//     sess.ExpectGet(getExpt)
//
// With the previous test setup, the following code ends up with rec == &record{ID: 1} and err nil:
//
//     rec := new(record)
//     err := sess.Get(rec, db.Cond{"id": 1})
//
// If no matches are found, this function panics with an ErrNoMatchingExpectations error.
func (s *Session) Get(record db.Record, cond interface{}) error {
	for i, e := range s.getExpectations {
		if cmp.Equal(e.ExpectedRecord, record) && cmp.Equal(e.ExpectedCond, cond) {
			if e.ReturnedRecord != nil {
				reflect.ValueOf(record).Elem().Set(reflect.Indirect(reflect.ValueOf(e.ReturnedRecord)))
			}

			s.getExpectations = append(s.getExpectations[:i], s.getExpectations[i+1:]...)

			return e.ReturnedError
		}
	}

	panic(fmt.Errorf("error matching session.Get: %w", ErrNoMatchingExpectations))
}

//ExpectGetArgs adds an expectation for the Get method, by separate arguments.
func (s *Session) ExpectGetArgs(record db.Record, cond interface{}, retRecord db.Record, retError error) {
	e := &SessionGetExpectation{
		ExpectedRecord: record,
		ExpectedCond:   cond,
		ReturnedRecord: retRecord,
		ReturnedError:  retError,
	}

	s.ExpectGet(e)
}

//ExpectGet adds an expectation for the Get method.
func (s *Session) ExpectGet(e *SessionGetExpectation) {
	if e != nil {
		s.getExpectations = append(s.getExpectations, e)
	}
}

// Save returns the first matching expectation from the list.
//
// This function panics with an ErrNoMatchingExpectations error if there are no such expectations.
func (s *Session) Save(record db.Record) error {
	for i, e := range s.saveExpectations {
		if cmp.Equal(e.ExpectedRecord, record) {
			if e.ReturnedRecord != nil {
				reflect.ValueOf(record).Elem().Set(reflect.Indirect(reflect.ValueOf(e.ReturnedRecord)))
			}

			s.saveExpectations = append(s.saveExpectations[:i], s.saveExpectations[i+1:]...)
			return e.ReturnedError
		}
	}

	panic(fmt.Errorf("error matching session.Save: %w", ErrNoMatchingExpectations))
}

//ExpectSaveArgs adds an expectation for the Save method, by separate arguments.
func (s *Session) ExpectSaveArgs(record db.Record, retRecord db.Record, retError error) {
	e := &SessionSaveExpectation{
		ExpectedRecord: record,
		ReturnedRecord: retRecord,
		ReturnedError:  retError,
	}

	s.ExpectSave(e)
}

//ExpectSave adds an expectation for the Save method.
func (s *Session) ExpectSave(e *SessionSaveExpectation) {
	if e != nil {
		s.saveExpectations = append(s.saveExpectations, e)
	}
}

// Delete returns the first matching expectation from the list.
//
// This function panics with an ErrNoMatchingExpectations error if there are no such expectations.
func (s *Session) Delete(record db.Record) error {
	for i, e := range s.deleteExpectations {
		if cmp.Equal(e.ExpectedRecord, record) {
			s.deleteExpectations = append(s.deleteExpectations[:i], s.deleteExpectations[i+1:]...)
			return e.ReturnedError
		}
	}

	panic(fmt.Errorf("error matching session.Delete: %w", ErrNoMatchingExpectations))
}

//ExpectDeleteArgs adds an expectation for the Delete method, by separate arguments.
func (s *Session) ExpectDeleteArgs(record db.Record, retError error) {
	e := &SessionDeleteExpectation{
		ExpectedRecord: record,
		ReturnedError:  retError,
	}

	s.ExpectDelete(e)
}

//ExpectDelete adds an expectation for the Delete method.
func (s *Session) ExpectDelete(e *SessionDeleteExpectation) {
	if e != nil {
		s.deleteExpectations = append(s.deleteExpectations, e)
	}
}

// ConnectionURL does nothing.
func (s *Session) ConnectionURL() db.ConnectionURL {
	return nil
}

// Ping is not implemented yet.
func (s *Session) Ping() error {
	panic(ErrUnimplemented)
}

// Collections is not implemented yet.
func (s *Session) Collections() ([]db.Collection, error) {
	panic(ErrUnimplemented)
}

// Reset does nothing.
func (s *Session) Reset() {}

// Close does nothing.
func (s *Session) Close() error {
	return nil
}

// Driver does nothing and always returns nil.
func (s *Session) Driver() interface{} {
	return nil
}

// Tx simply executes the provided function and returns its result.
func (s *Session) Tx(fn func(db.Session) error) error {
	return fn(s)
}

// TxContext simply executes the provided function and returns its result.
func (s *Session) TxContext(_ context.Context, fn func(db.Session) error, _ *sql.TxOptions) error {
	return fn(s)
}

// Context returns a new background context.
func (s *Session) Context() context.Context {
	return context.Background()
}

// WithContext does nothing and simply returns the current session.
func (s *Session) WithContext(ctx context.Context) db.Session {
	return s
}

// SetPreparedStatementCache does nothing.
func (s *Session) SetPreparedStatementCache(bool) {}

// PreparedStatementCacheEnabled does nothing and always returns false.
func (s *Session) PreparedStatementCacheEnabled() bool {
	return false
}

// SetConnMaxLifetime does nothing.
func (s *Session) SetConnMaxLifetime(time.Duration) {}

// ConnMaxLifetime does nothing and always returns one second.
func (s *Session) ConnMaxLifetime() time.Duration {
	return time.Second
}

// SetMaxIdleConns does nothing.
func (s *Session) SetMaxIdleConns(int) {}

// MaxIdleConns does nothing and always returns 0.
func (s *Session) MaxIdleConns() int {
	return 0
}

// SetMaxOpenConns does nothing.
func (s *Session) SetMaxOpenConns(int) {}

// MaxOpenConns does nothing and always returns 0.
func (s *Session) MaxOpenConns() int {
	return 0
}

// SetMaxTransactionRetries does nothing.
func (s *Session) SetMaxTransactionRetries(int) {}

// MaxTransactionRetries does nothing and always returns 0.
func (s *Session) MaxTransactionRetries() int {
	return 0
}

var _ = db.Session(&Session{})

// SessionGetExpectation defines the expected and returned values for the Get method.
type SessionGetExpectation struct {
	ExpectedRecord db.Record
	ExpectedCond   interface{}
	ReturnedRecord db.Record
	ReturnedError  error
}

// SessionSaveExpectation defines the expected and returned values for the Save method.
type SessionSaveExpectation struct {
	ExpectedRecord db.Record
	ReturnedRecord db.Record
	ReturnedError  error
}

//SessionDeleteExpectation defines the expected and returned values for the Delete method.
type SessionDeleteExpectation struct {
	ExpectedRecord db.Record
	ReturnedError  error
}

// ErrNoMatchingExpectations defines the error returned when no expectations could be matched to the
// current action.
var ErrNoMatchingExpectations = errors.New("no matching expectations")

// ErrUnimplemented defines the error returned when a method is not yet implemented.
var ErrUnimplemented = errors.New("unimplemented")
