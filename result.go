package testadapter

import (
	"fmt"
	"reflect"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/upper/db/v4"
)

// Result defines an upper.io result used in tests to simulate a database.
type Result struct {
	collection  *Collection
	currentArgs *resultArgs
	t           *testing.T
}

func NewResult(args ...interface{}) *Result {
	return &Result{
		currentArgs: &resultArgs{
			findArgs: args,
		},
	}
}

// Limit adds an argument expectation to the result.
func (r *Result) Limit(arg int) db.Result {
	r.currentArgs.limitArgs = append(r.currentArgs.limitArgs, arg)
	return r
}

// Offset adds an argument expectation to the result.
func (r *Result) Offset(arg int) db.Result {
	r.currentArgs.offsetArgs = append(r.currentArgs.offsetArgs, arg)
	return r
}

// OrderBy adds an argument expectation to the result.
func (r *Result) OrderBy(args ...interface{}) db.Result {
	r.currentArgs.orderByArgs = append(r.currentArgs.orderByArgs, args)
	return r
}

// Select adds an argument expectation to the result.
func (r *Result) Select(args ...interface{}) db.Result {
	r.currentArgs.selectArgs = append(r.currentArgs.selectArgs, args)
	return r
}

// And adds an argument expectation to the result.
func (r *Result) And(args ...interface{}) db.Result {
	r.currentArgs.andArgs = append(r.currentArgs.selectArgs, args)
	return r
}

// GroupBy adds an argument expectation to the result.
func (r *Result) GroupBy(args ...interface{}) db.Result {
	r.currentArgs.groupByArgs = append(r.currentArgs.groupByArgs, args)
	return r
}

// Paginate adds an argument expectation to the result.
func (r *Result) Paginate(arg uint) db.Result {
	r.currentArgs.paginateArgs = append(r.currentArgs.paginateArgs, arg)
	return r
}

// Page adds an argument expectation to the result.
func (r *Result) Page(arg uint) db.Result {
	r.currentArgs.pageArgs = append(r.currentArgs.pageArgs, arg)
	return r
}

// Cursor adds an argument expectation to the result.
func (r *Result) Cursor(arg string) db.Result {
	r.currentArgs.cursorArgs = append(r.currentArgs.cursorArgs, arg)
	return r
}

// NextPage adds an argument expectation to the result.
func (r *Result) NextPage(arg interface{}) db.Result {
	r.currentArgs.nextPageArgs = append(r.currentArgs.nextPageArgs, arg)
	return r
}

// PrevPage adds an argument expectation to the result.
func (r *Result) PrevPage(arg interface{}) db.Result {
	r.currentArgs.prevPageArgs = append(r.currentArgs.prevPageArgs, arg)
	return r
}

// Count returns the first expectation from the list.
//
// This function panics with an ErrNoMatchingExpectations error if there are no such expectations.
func (r *Result) Count() (uint64, error) {
	for i, e := range r.collection.resultCountExpectations {
		if r.matchesArgs(e.expectedArgs) {
			r.collection.resultCountExpectations = append(r.collection.resultCountExpectations[:i], r.collection.resultCountExpectations[i+1:]...)
			return e.ReturnedResult, e.ReturnedError
		}
	}

	panic(fmt.Errorf("error matching result.Count: %w", ErrNoMatchingExpectations))
}

// ExpectCountArgs adds an expectation for the Count method, by separate arguments.
func (r *Result) ExpectCountArgs(expectedResult uint64, expectedError error) {
	e := &ResultCountExpectation{
		expectedArgs:   r.currentArgs,
		ReturnedResult: expectedResult,
		ReturnedError:  expectedError,
	}

	r.ExpectCount(e)
}

// ExpectCount adds an expectation for the Count method.
func (r *Result) ExpectCount(e *ResultCountExpectation) {
	if e != nil {
		r.collection.resultCountExpectations = append(r.collection.resultCountExpectations, e)
	}
}

// Exists returns the first expectation from the list.
//
// This function panics with an ErrNoMatchingExpectations error if there are no such expectations.
func (r *Result) Exists() (bool, error) {
	for i, e := range r.collection.resultExistsExpectations {
		if r.matchesArgs(e.expectedArgs) {
			r.collection.resultExistsExpectations = append(r.collection.resultExistsExpectations[:i], r.collection.resultExistsExpectations[i+1:]...)
			return e.ReturnedResult, e.ReturnedError
		}
	}

	panic(fmt.Errorf("error matching result.Exists: %w", ErrNoMatchingExpectations))
}

// ExpectExistsArgs adds an expectation for the Exists method, by separate arguments.
func (r *Result) ExpectExistsArgs(expectedResult bool, expectedError error) {
	e := &ResultExistsExpectation{
		expectedArgs:   r.currentArgs,
		ReturnedResult: expectedResult,
		ReturnedError:  expectedError,
	}

	r.ExpectExists(e)
}

// ExpectExists adds an expectation for the Exists method.
func (r *Result) ExpectExists(e *ResultExistsExpectation) {
	if e != nil {
		r.collection.resultExistsExpectations = append(r.collection.resultExistsExpectations, e)
	}
}

// All returns the first matching expectation from the list, replacing the specified slice with the
// returned items.
//
// This function panics with an ErrNoMatchingExpectations error if there are no such expectations.
func (r *Result) All(slice interface{}) error {
	for i, e := range r.collection.resultAllExpectations {
		if r.matchesArgs(e.expectedArgs) && cmp.Equal(e.ExpectedSlice, slice) {
			if e.ReturnedSlice != nil {
				reflect.ValueOf(slice).Elem().Set(reflect.ValueOf(e.ReturnedSlice))
			}

			r.collection.resultAllExpectations = append(r.collection.resultAllExpectations[:i], r.collection.resultAllExpectations[i+1:]...)

			return e.ReturnedError
		}
	}

	panic(fmt.Errorf("error matching result.All: %w", ErrNoMatchingExpectations))
}

// ExpectAllArgs adds an expectation for the All method, by separate arguments.
func (r *Result) ExpectAllArgs(slice interface{}, retSlice interface{}, retError error) {
	e := &ResultAllExpectation{
		ExpectedSlice: slice,
		ReturnedSlice: retSlice,
		ReturnedError: retError,
	}

	r.ExpectAll(e)
}

// ExpectAll adds an expectation for the All method.
func (r *Result) ExpectAll(e *ResultAllExpectation) {
	if e != nil {
		e.expectedArgs = r.currentArgs
		r.collection.resultAllExpectations = append(r.collection.resultAllExpectations, e)
	}
}

// One returns the first matching expectation from the list, replacing the specified item with the
// returned one.
//
// This function panics with an ErrNoMatchingExpectations error if there are no such expectations.
func (r *Result) One(item interface{}) error {
	for i, e := range r.collection.resultOneExpectations {
		if r.matchesArgs(e.expectedArgs) && cmp.Equal(e.ExpectedItem, item) {
			if e.ReturnedItem != nil {
				reflect.ValueOf(item).Elem().Set(reflect.Indirect(reflect.ValueOf(e.ReturnedItem)))
			}

			r.collection.resultOneExpectations = append(r.collection.resultOneExpectations[:i], r.collection.resultOneExpectations[i+1:]...)

			return e.ReturnedError
		}
	}

	panic(fmt.Errorf("error matching result.One: %w", ErrNoMatchingExpectations))
}

// ExpectOneArgs adds an expectation for the One method, by separate arguments.
func (r *Result) ExpectOneArgs(item interface{}, retItem interface{}, retError error) {
	e := &ResultOneExpectation{
		ExpectedItem:  item,
		ReturnedItem:  retItem,
		ReturnedError: retError,
	}

	r.ExpectOne(e)
}

// ExpectOne adds an expectation for the One method.
func (r *Result) ExpectOne(e *ResultOneExpectation) {
	if e != nil {
		e.expectedArgs = r.currentArgs
		r.collection.resultOneExpectations = append(r.collection.resultOneExpectations, e)
	}
}

// Update returns the first matching expectation from the list.
//
// This function panics with an ErrNoMatchingExpectations error if there are no such expectations.
func (r *Result) Update(item interface{}) error {
	for i, e := range r.collection.resultUpdateExpectations {
		if r.matchesArgs(e.expectedArgs) && cmp.Equal(e.ExpectedItem, item) {
			if e.ReturnedItem != nil {
				reflect.ValueOf(item).Elem().Set(reflect.Indirect(reflect.ValueOf(e.ReturnedItem)))
			}

			r.collection.resultUpdateExpectations = append(r.collection.resultUpdateExpectations[:i], r.collection.resultUpdateExpectations[i+1:]...)
			return e.ReturnedError
		}
	}

	panic(fmt.Errorf("error matching result.Update: %w", ErrNoMatchingExpectations))
}

// ExpectUpdateArgs adds an expectation for the Update method, by separate arguments.
func (r *Result) ExpectUpdateArgs(item interface{}, retItem interface{}, retError error) {
	e := &ResultUpdateExpectation{
		ExpectedItem:  item,
		ReturnedItem:  retItem,
		ReturnedError: retError,
	}

	r.ExpectUpdate(e)
}

// ExpectUpdate adds an expectation for the Update method.
func (r *Result) ExpectUpdate(e *ResultUpdateExpectation) {
	if e != nil {
		e.expectedArgs = r.currentArgs
		r.collection.resultUpdateExpectations = append(r.collection.resultUpdateExpectations, e)
	}
}

// Delete returns the first matching expectation from the list.
//
// This function panics with an ErrNoMatchingExpectations error if there are no such expectations.
func (r *Result) Delete() error {
	for i, e := range r.collection.resultDeleteExpectations {
		if r.matchesArgs(e.expectedArgs) {
			r.collection.resultDeleteExpectations = append(r.collection.resultDeleteExpectations[:i], r.collection.resultDeleteExpectations[i+1:]...)
			return e.ReturnedError
		}
	}

	panic(fmt.Errorf("error matching result.Delete: %w", ErrNoMatchingExpectations))
}

// ExpectDeleteArgs adds an expectation for the Delete method, by separate arguments.
func (r *Result) ExpectDeleteArgs(retError error) {
	e := &ResultDeleteExpectation{
		ReturnedError: retError,
	}

	r.ExpectDelete(e)
}

// ExpectDelete adds an expectation for the Delete method.
func (r *Result) ExpectDelete(e *ResultDeleteExpectation) {
	if e != nil {
		e.expectedArgs = r.currentArgs
		r.collection.resultDeleteExpectations = append(r.collection.resultDeleteExpectations, e)
	}
}

// Next is not implemented yet.
func (r *Result) Next(ptrToStruct interface{}) bool {
	panic(ErrUnimplemented)
}

// Err is not implemented yet.
func (r *Result) Err() error {
	panic(ErrUnimplemented)
}

// TotalPages is not implemented yet.
func (r *Result) TotalPages() (uint, error) {
	panic(ErrUnimplemented)
}

// TotalEntries is not implemented yet.
func (r *Result) TotalEntries() (uint64, error) {
	panic(ErrUnimplemented)
}

// String is not implemented yet.
func (r *Result) String() string {
	panic(ErrUnimplemented)
}

// Close does nothing.
func (r *Result) Close() error {
	return nil
}

var _ = db.Result(&Result{})

func (r *Result) matchesArgs(expectedArgs *resultArgs) bool {
	return cmp.Equal(expectedArgs, r.currentArgs, cmp.AllowUnexported(resultArgs{}))
}

type resultArgs struct {
	findArgs     []interface{}
	limitArgs    []int
	offsetArgs   []int
	orderByArgs  [][]interface{}
	selectArgs   [][]interface{}
	andArgs      [][]interface{}
	groupByArgs  [][]interface{}
	paginateArgs []uint
	pageArgs     []uint
	cursorArgs   []string
	nextPageArgs []interface{}
	prevPageArgs []interface{}
}

// ResultCountExpectation defines the returned values for the Count method.
type ResultCountExpectation struct {
	expectedArgs   *resultArgs
	ReturnedResult uint64
	ReturnedError  error
}

// ResultExistsExpectation defines the returned values for the Exists method.
type ResultExistsExpectation struct {
	expectedArgs   *resultArgs
	ReturnedResult bool
	ReturnedError  error
}

// ResultAllExpectation defines the expected and returned values for the All method.
type ResultAllExpectation struct {
	expectedArgs  *resultArgs
	ExpectedSlice interface{}
	ReturnedSlice interface{}
	ReturnedError error
}

// ResultOneExpectation defines the expected and returned values for the One method.
type ResultOneExpectation struct {
	expectedArgs  *resultArgs
	ExpectedItem  interface{}
	ReturnedItem  interface{}
	ReturnedError error
}

// ResultUpdateExpectation defines the expected and returned values for the Update method.
type ResultUpdateExpectation struct {
	expectedArgs  *resultArgs
	ExpectedItem  interface{}
	ReturnedItem  interface{}
	ReturnedError error
}

// ResultDeleteExpectation defines the returned values for the Delete method.
type ResultDeleteExpectation struct {
	expectedArgs  *resultArgs
	ReturnedError error
}
