package testadapter

import (
	"errors"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/upper/db/v4"
)

func TestSessionNewResult(t *testing.T) {
	countResult := NewResult()
	oneResult := NewResult(db.Cond{"id": 1}).And(db.Cond{"code": "value"})

	sess := NewSession(t)
	NewCollection(sess, "coll").WithResult(countResult).ExpectCountArgs(10, nil)
	NewCollection(sess, "coll").WithResult(oneResult).ExpectOneArgs(&testRecord{}, &testRecord{ID: 1}, nil)

	returnedCount, err := sess.Collection("coll").Find().Count()
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	} else if returnedCount != 10 {
		t.Fatalf("expected returned count to be 10, but was %d", returnedCount)
	}

	item := &testRecord{}
	if err := sess.Collection("coll").Find(db.Cond{"id": 1}).And(db.Cond{"code": "value"}).One(item); err != nil {
		t.Fatalf("unexpected error: %s", err)
	} else if !cmp.Equal(&testRecord{ID: 1}, item) {
		t.Fatalf("expected returned item to be %v, but was %v", testRecord{ID: 1}, item)
	}
}

func TestResultExpectOne(t *testing.T) {
	tests := map[string]struct {
		dbResultOneExpts   []*ResultOneExpectation
		expectedNotToMatch bool
		expectedItem       *testRecord
		expectedError      error
	}{
		"whenExpectationMatches": {
			dbResultOneExpts: []*ResultOneExpectation{
				{
					ExpectedItem:  &testRecord{},
					ReturnedItem:  &testRecord{ID: 1},
					ReturnedError: errTest,
				},
			},
			expectedItem:  &testRecord{ID: 1},
			expectedError: errTest,
		},
		"whenExpectationDoesntMatchItem": {
			dbResultOneExpts: []*ResultOneExpectation{
				{
					ExpectedItem:  &testRecord{ID: -1},
					ReturnedItem:  &testRecord{ID: 1},
					ReturnedError: nil,
				},
			},
			expectedNotToMatch: true,
		},
		"whenReturnedItemIsNil": {
			dbResultOneExpts: []*ResultOneExpectation{
				{
					ExpectedItem:  &testRecord{},
					ReturnedItem:  nil,
					ReturnedError: nil,
				},
			},
			expectedItem:  &testRecord{},
			expectedError: nil,
		},
		"whenMultipleExpectationsAndTheFirstMatches": {
			dbResultOneExpts: []*ResultOneExpectation{
				{
					ExpectedItem:  &testRecord{},
					ReturnedItem:  &testRecord{ID: 1},
					ReturnedError: nil,
				},
				{
					ExpectedItem:  &testRecord{},
					ReturnedItem:  &testRecord{ID: 2},
					ReturnedError: nil,
				},
			},
			expectedItem:  &testRecord{ID: 1},
			expectedError: nil,
		},
		"whenMultipleExpectationsAndTheSecondMatches": {
			dbResultOneExpts: []*ResultOneExpectation{
				{
					ExpectedItem:  &testRecord{ID: -1},
					ReturnedItem:  &testRecord{ID: 2},
					ReturnedError: nil,
				},
				{
					ExpectedItem:  &testRecord{},
					ReturnedItem:  &testRecord{ID: 1},
					ReturnedError: nil,
				},
			},
			expectedItem:  &testRecord{ID: 1},
			expectedError: nil,
		},
		"whenMultipleExpectationsButNoneMatch": {
			dbResultOneExpts: []*ResultOneExpectation{
				{
					ExpectedItem:  &testRecord{ID: -1},
					ReturnedItem:  &testRecord{ID: 2},
					ReturnedError: nil,
				},
				{
					ExpectedItem:  &testRecord{ID: -2},
					ReturnedItem:  &testRecord{ID: 3},
					ReturnedError: nil,
				},
			},
			expectedNotToMatch: true,
		},
		"whenNoExpectations": {
			dbResultOneExpts:   []*ResultOneExpectation{},
			expectedNotToMatch: true,
		},
		"whenAddedExpectationIsNil": {
			dbResultOneExpts:   []*ResultOneExpectation{nil},
			expectedNotToMatch: true,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			sess := NewSession(t)
			for _, e := range tt.dbResultOneExpts {
				sess.Collection("coll").Find().(*Result).ExpectOne(e)
			}

			var item *testRecord
			var err error
			var panicResult interface{}
			foundMatch := func() bool {
				defer func() { panicResult = recover() }()

				item = new(testRecord)
				err = sess.Collection("coll").Find().One(item)
				return true
			}()

			if tt.expectedNotToMatch {
				if foundMatch {
					t.Fatal("expected not to find match, but did")
				}
				return
			} else if !foundMatch {
				if e, ok := panicResult.(error); ok && errors.Is(e, ErrNoMatchingExpectations) {
					t.Fatal("expected to find match, but didn't")
				} else {
					t.Fatalf("function panic'ed: %v", panicResult)
				}
			}

			if !cmp.Equal(tt.expectedItem, item) {
				t.Fatalf("expected item to be different:\n%s", cmp.Diff(tt.expectedItem, item))
			}

			if !errorsEqual(tt.expectedError, err) {
				t.Fatalf("expected error to be different:\n%s", cmp.Diff(tt.expectedError, err))
			}
		})
	}
}
