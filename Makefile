# Run code style tests
.PHONY: test-lint
test-lint:
	@docker run \
	  --workdir /app \
	  --volume ${PWD}:/app \
	  --rm \
	  golangci/golangci-lint:latest-alpine \
	    golangci-lint run

# Run tests
.PHONY: test
test:
	go test ${TEST_ARGS} -race ./...
