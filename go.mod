module gitlab.com/zyrthofar/testadapter

go 1.16

require (
	github.com/google/go-cmp v0.5.5
	github.com/upper/db/v4 v4.1.0
)
