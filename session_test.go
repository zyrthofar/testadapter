package testadapter

import (
	"errors"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/upper/db/v4"
)

type testRecord struct {
	ID int
}

func (r *testRecord) Store(sess db.Session) db.Store {
	return nil
}

var errTest = errors.New("test")

func TestSessionExpectGet(t *testing.T) {
	tests := map[string]struct {
		dbSessionGetExpts  []*SessionGetExpectation
		expectedNotToMatch bool
		expectedRecord     *testRecord
		expectedError      error
	}{
		"whenExpectationMatches": {
			dbSessionGetExpts: []*SessionGetExpectation{
				{
					ExpectedRecord: &testRecord{},
					ExpectedCond:   db.Cond{"id": 1},
					ReturnedRecord: &testRecord{ID: 1},
					ReturnedError:  errTest,
				},
			},
			expectedRecord: &testRecord{ID: 1},
			expectedError:  errTest,
		},
		"whenExpectationDoesntMatchRecord": {
			dbSessionGetExpts: []*SessionGetExpectation{
				{
					ExpectedRecord: &testRecord{ID: -1},
					ExpectedCond:   db.Cond{"id": 1},
					ReturnedRecord: &testRecord{ID: 1},
					ReturnedError:  nil,
				},
			},
			expectedNotToMatch: true,
		},
		"whenExpectationDoesntMatchCond": {
			dbSessionGetExpts: []*SessionGetExpectation{
				{
					ExpectedRecord: &testRecord{},
					ExpectedCond:   db.Cond{"id": 2},
					ReturnedRecord: &testRecord{ID: 1},
					ReturnedError:  nil,
				},
			},
			expectedNotToMatch: true,
		},
		"whenReturnedRecordIsNil": {
			dbSessionGetExpts: []*SessionGetExpectation{
				{
					ExpectedRecord: &testRecord{},
					ExpectedCond:   db.Cond{"id": 1},
					ReturnedRecord: nil,
					ReturnedError:  nil,
				},
			},
			expectedRecord: &testRecord{},
			expectedError:  nil,
		},
		"whenMultipleExpectationsAndTheFirstMatches": {
			dbSessionGetExpts: []*SessionGetExpectation{
				{
					ExpectedRecord: &testRecord{},
					ExpectedCond:   db.Cond{"id": 1},
					ReturnedRecord: &testRecord{ID: 1},
					ReturnedError:  nil,
				},
				{
					ExpectedRecord: &testRecord{},
					ExpectedCond:   db.Cond{"id": 2},
					ReturnedRecord: &testRecord{ID: 2},
					ReturnedError:  nil,
				},
			},
			expectedRecord: &testRecord{ID: 1},
			expectedError:  nil,
		},
		"whenMultipleExpectationsAndTheSecondMatches": {
			dbSessionGetExpts: []*SessionGetExpectation{
				{
					ExpectedRecord: &testRecord{},
					ExpectedCond:   db.Cond{"id": 2},
					ReturnedRecord: &testRecord{ID: 2},
					ReturnedError:  nil,
				},
				{
					ExpectedRecord: &testRecord{},
					ExpectedCond:   db.Cond{"id": 1},
					ReturnedRecord: &testRecord{ID: 1},
					ReturnedError:  nil,
				},
			},
			expectedRecord: &testRecord{ID: 1},
			expectedError:  nil,
		},
		"whenMultipleExpectationsButNoneMatch": {
			dbSessionGetExpts: []*SessionGetExpectation{
				{
					ExpectedRecord: &testRecord{},
					ExpectedCond:   db.Cond{"id": 2},
					ReturnedRecord: &testRecord{ID: 2},
					ReturnedError:  nil,
				},
				{
					ExpectedRecord: &testRecord{},
					ExpectedCond:   db.Cond{"id": 3},
					ReturnedRecord: &testRecord{ID: 3},
					ReturnedError:  nil,
				},
			},
			expectedNotToMatch: true,
		},
		"whenNoExpectations": {
			dbSessionGetExpts:  []*SessionGetExpectation{},
			expectedNotToMatch: true,
		},
		"whenAddedExpectationIsNil": {
			dbSessionGetExpts:  []*SessionGetExpectation{nil},
			expectedNotToMatch: true,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			sess := NewSession(t)
			for _, e := range tt.dbSessionGetExpts {
				sess.ExpectGet(e)
			}

			var record *testRecord
			var err error
			var panicResult interface{}
			foundMatch := func() bool {
				defer func() { panicResult = recover() }()

				record = new(testRecord)
				err = sess.Get(record, db.Cond{"id": 1})
				return true
			}()

			if tt.expectedNotToMatch {
				if foundMatch {
					t.Fatal("expected not to find match, but did")
				}
				return
			} else if !foundMatch {
				if e, ok := panicResult.(error); ok && errors.Is(e, ErrNoMatchingExpectations) {
					t.Fatal("expected to find match, but didn't")
				} else {
					t.Fatalf("function panic'ed: %v", panicResult)
				}
			}

			if !cmp.Equal(tt.expectedRecord, record) {
				t.Fatalf("expected record to be different:\n%s", cmp.Diff(tt.expectedRecord, record))
			}

			if !errorsEqual(tt.expectedError, err) {
				t.Fatalf("expected error to be different:\n%s", cmp.Diff(tt.expectedError, err))
			}
		})
	}
}

func TestSessionExpectGetArgs(t *testing.T) {
	sess := NewSession(t)
	sess.ExpectGetArgs(&testRecord{}, db.Cond{"id": 1}, &testRecord{ID: 1}, nil)

	record := &testRecord{}
	if err := sess.Get(record, db.Cond{"id": 1}); err != nil {
		t.Fatalf("unexpected error: %s", err)
	}

	expectedRecord := &testRecord{ID: 1}
	if !cmp.Equal(expectedRecord, record) {
		t.Fatalf("expected record to be different:\n%s", cmp.Diff(expectedRecord, record))
	}
}

func TestSessionExpectGetToRemoveExpectations(t *testing.T) {
	sess := NewSession(t)
	sess.ExpectGetArgs(&testRecord{}, db.Cond{"id": 1}, &testRecord{ID: 10}, nil)
	sess.ExpectGetArgs(&testRecord{}, db.Cond{"id": 1}, &testRecord{ID: 20}, nil)

	firstRecord := &testRecord{}
	if err := sess.Get(firstRecord, db.Cond{"id": 1}); err != nil {
		t.Fatalf("unexpected error: %s", err)
	}

	if !cmp.Equal(&testRecord{ID: 10}, firstRecord) {
		t.Fatalf("unexpected first record: %v", firstRecord)
	}

	secondRecord := &testRecord{}
	if err := sess.Get(secondRecord, db.Cond{"id": 1}); err != nil {
		t.Fatalf("unexpected error: %s", err)
	}

	if cmp.Equal(&testRecord{ID: 10}, secondRecord) {
		t.Fatalf("expected first expectation to be removed, but wasn't")
	} else if !cmp.Equal(&testRecord{ID: 20}, secondRecord) {
		t.Fatalf("unexpected second record: %v", secondRecord)
	}
}

func TestSessionExpectSave(t *testing.T) {

}

func TestSessionExpectSaveArgs(t *testing.T) {

}

func TestSessionExpectDelete(t *testing.T) {

}

func TestSessionExpectDeleteArgs(t *testing.T) {

}

func errorsEqual(err1, err2 error) bool {
	if err1 == nil {
		return err2 == nil
	} else if err2 == nil {
		return false
	}

	return err1.Error() == err2.Error()
}
